int searchSmallest(List<int> list) {
  int idx = 0;
  int smallItem = list[idx]; 
  
  list.asMap().forEach((key, el) => {
   if (smallItem > el) {
     smallItem = el,
     idx = key
   }
  });
  
  return idx;
}

List<int> selectionSort(List<int> list) {
  List<int> result = [];
  while (list.isNotEmpty) {
    result.add(list.removeAt(searchSmallest(list)));
  }
  return result;
}
