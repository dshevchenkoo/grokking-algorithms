maxValue(List list) {
  if (list.length == 2) {
    return list[1] > list[0] ? list[1] : list[0];
  }
  
  var current = list.removeAt(0);
  return maxValue([current, maxValue(list)]);
}
