binarySearch(List list, el, [startPosition = 0]) {
  var mid = (startPosition + (list.length - 1)) ~/ 2;
  
  if (list[mid] == el) {
    return mid;
  } else if (el > list[mid]) {
    startPosition = mid;
  } else if (el < list[mid]) {
    list = list.sublist(startPosition, mid);
  }
  
  return binarySearch(list, el, startPosition);
}
