sum(List numberList) {
  if (numberList.length <= 1) {
    return numberList[0];
  }
  
 return numberList.removeAt(0) + sum(numberList);
}
