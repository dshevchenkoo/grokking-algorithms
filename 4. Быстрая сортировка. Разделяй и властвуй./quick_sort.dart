quickSort(List list) {
  if (list.length < 2) {
    return list;
  }
  
  var pivot = list.removeAt(0);
  var less = list.where((el) => el <= pivot).toList();
  var greater = list.where((el) => el > pivot).toList();
  
  return quickSort(less) + [pivot] + quickSort(greater);  
}
