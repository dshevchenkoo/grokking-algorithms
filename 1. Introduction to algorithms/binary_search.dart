int? binarySearch(List<int> list, int searchNumber) {
  
  final getMid = (low, last) => (last + low) ~/ 2;
 
  int low = list[0];
  int last = list[list.length - 1];
 
  while(low <= last) {
    
    int mid = getMid(low, last);
    
    if (list[mid] == searchNumber) {
      return mid;
    } else if (searchNumber > list[mid]) {
      low = mid + 1;
    } else {
      last = mid - 1;
    }
  }
  return null;
}
